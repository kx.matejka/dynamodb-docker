AWS DynamoDB
============

This image is for local development only. Full documentation [here](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/DynamoDBLocal.html)

# Setup

## Docker

### Build image
```sh
docker build -t dynamodb .
```

### Run container

container will expose port **8000** and save persistent data to the **data** directory
```sh
docker run -it -p 8000:8000 -v "`pwd`/data:/data" dynamodb
```

## AWS CLI

### Create table

this command will create a table named **default** with key **id**
```
aws dynamodb create-table --cli-input-json file://default-scheme.json --endpoint-url http://localhost:8000
```

### List tables
```sh
aws dynamodb list-tables --endpoint-url http://localhost:8000
```

### Scan table
```sh
aws dynamodb scan --table-name default --endpoint-url http://localhost:8000
```

## AWS NODE.JS SDK

aws sdk needs to specify an url of local endpoint
```js
const aws = require('aws-sdk')
const dynamo = new aws.DynamoDB({endpoint: 'http://localhost:8000'})
```
